import React, { useEffect, useState } from 'react'

function Movies() {
  const [ movies, setMovies ] = useState([]);

  const fetchMovies = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/movies`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
      if (response.ok) {
        const data = await response.json();
        setMovies(data);
      }
    } catch(err) {
        console.log(err);
    }
  }

  useEffect(() => {
    fetchMovies();
  }, [])
  return (
    <div>Movies</div>
  )
}

export default Movies