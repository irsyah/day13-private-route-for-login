import React, { useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom';

function Login() {
  const location = useLocation();
  const navigate = useNavigate();

  const [ userLogin, setUserLogin ] = useState({ email: '', password: ''});

  const handleOnChange = (e) => {
    setUserLogin(currUser => {
        return { ...currUser, [e.target.id]: e.target.value }
    })
  }

  const signIn = async (e) => {
    e.preventDefault();
    
    try {
        const response = await fetch(`http://localhost:8080/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userLogin)
        })

        if (response.ok) {
            const data = await response.json();
            localStorage.setItem('token', data.jwtToken);
           
            if (location.state) {
                navigate(`${location.state.from.pathname}`)
            } else {
                navigate('/');
            }
        }
    } catch(err) {
        console.log(err);
    }
  }

  
  return (
    <div className="container">
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <h2 className="text-center text-dark mt-5">Login Page</h2>
                    <div className="card my-5">

                        <form className="card-body cardbody-color p-lg-5">

                            <div className="text-center">
                                <img src="https://i.ytimg.com/vi/dqNtBrVZQR4/mqdefault.jpg"
                                     className="img-fluid profile-image-pic img-thumbnail rounded-circle my-3"
                                     width="200px" alt="profile"/>
                            </div>

                            <div className="mb-3">
                                <input type="text" className="form-control" id="email" aria-describedby="emailHelp" placeholder='email'
                                       onChange={handleOnChange} value={userLogin.email}/>
                            </div>
                            <div className="mb-3">
                                <input type="password" className="form-control" id="password" placeholder="password"
                                       onChange={handleOnChange} value={userLogin.password}/>
                            </div>
                            <div className="text-center">
                                <button type="submit" onClick={signIn}
                                        className="btn btn-warning btn-color px-5 mb-5 w-100">Login
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <div id="emailHelp" className="form-text text-center mb-5 text-dark">Not
                                    Registered? <a href="#" className="text-dark fw-bold"> Create an Account</a>
                            </div>
            </div>
        </div>
  )
}

export default Login