import { Routes, Route } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

import PrivateRoute from './components/PrivateRoute';
import Home from './pages/Home';
import Login from './pages/Login';
import Movies from './pages/Movies';

function App() {
  const navigate = useNavigate();
  
  const signOut = () => {
    localStorage.removeItem('token');
    navigate('/login');
  }

  return (
    <div>

      <Routes>
        <Route path='/' element={
          <PrivateRoute>
            <Home />
          </PrivateRoute>
        } />
        <Route path='/movies' element={
          <PrivateRoute>
            <Movies />
          </PrivateRoute>
        } />
        <Route path='/login' element={<Login />} />
      </Routes>

      {localStorage.getItem('token') && <button onClick={signOut}>Logout</button>}
    </div>
  );
}

export default App;
